import initCodeAnalyticsApp from 'ee/analytics/code_analytics/index';

document.addEventListener('DOMContentLoaded', initCodeAnalyticsApp);
